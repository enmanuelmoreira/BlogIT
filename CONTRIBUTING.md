## How to contribute to this project

First, fork this repository by clicking the fork button.

Next, clone your forked repo.

```
git clone https://gitlab.com/enmanuelmoreira/BlogIT.git
cd BlogIT
```

Then, install the dev dependencies.

```
npm install
```

And now you are ready to go!

Here are some useful commands.

```
npm run babel #build theme.js with babel
npm run build #build theme.js and then build the static site
npm run start #build theme.js and then run a local debugging server
npm run start-production #build theme.js and then run a local debugging server in production environment
npm run serve #run a local debugging server without building a new theme.js
npm run serve-production #run a local debugging server in production environment without building a new theme.js
```

Finally, create a new merge request at https://gitlab.com/enmanuelmoreira/BlogIT/-/merge_requests to submit your contribution🎉
