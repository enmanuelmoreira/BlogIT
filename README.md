# BlogIT Theme | Hugo

[![GitLab tag (latest by date)](https://img.shields.io/gitlab/v/tag/enmanuelmoreira/BlogIT?style=flat-square)](https://gitlab.com/enmanuelmoreira/BlogIT/-/releases)
[![Hugo](https://img.shields.io/badge/Hugo-%5E0.83.0-ff4088?style=flat-square&logo=hugo)](https://gohugo.io/)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/enmanuelmoreira/BlogIT/-/blob/main/LICENSE)
[![Netlify Status](https://api.netlify.com/api/v1/badges/88424eff-e512-4606-b25d-3a0aef6fe5cb/deploy-status)](https://app.netlify.com/sites/hugoblogit/deploys)
> **BlogIT** is a **clean**, **elegant** but **advanced** blog theme for [Hugo](https://gohugo.io/).

It is based on the original [LoveIt Theme](https://github.com/dillonzq/LoveIt), and based on [DoIt Theme](https://github.com/HEIGE-PCloud/DoIt)

Since the three themes have a similar look, if you have questions about their differences,
read [Why choose BlogIT](#why-choose-BlogIT) so that you can choose the one that works best for you.

The goal of BlogIT theme is to create a powerful and easy-to-use Hugo theme, so you can go straight into your blog content creation and just **do it** without worrying about the complex technical details.

## Getting started

Head to this [documentation page](https://hugoblogit.netlify.app/theme-documentation-basics/) for a complete guidence to get started with the BlogIT theme.

## Migrating from LoveIt

If you are currently using the LoveIt theme, it is very easy to migrate to BlogIT.

You can add this repo as a submodule of your site directory.

```bash
git submodule add https://gitlab.com/enmanuelmoreira/BlogIT.git themes/BlogIT
```

And later you can update the submodule in your site directory to the latest commit using this command:

```bash
git submodule update --remote --merge
```

Alternatively, you can download the [latest release .zip file](https://gitlab.com/enmanuelmoreira/BlogIT/releases) of the theme and extract it in the themes directory. (Not recommended, the repo is being updated frequently so the releases may out of date.)

Next, go to the `config.toml` and change the default theme to `BlogIT`.

```diff
- theme = "LoveIt"
+ theme = "BlogIT"
```

Now the migration is finished and everything is ready 🎉

## Theme Installation Options

### Option 1 (recommended): Adding the Theme as a Hugo Module

Use this option if you want to pull in the theme files from the main Bilberry Hugo theme repository.
This option makes it easy to keep the theme up to date in your site.

- Initialize your website as a Hugo module from the site's root:

```shell
cd my-new-blog
hugo mod init github.com/<your-user>/my-new-blog
```

Following the Hugo module initialization, you may have the following warning: module "
gitlab.com/enmanuelmoreira/BlogIT/v2" not found, which should be ignored.

- Pull theme files to add new content to your website:

```shell
hugo mod vendor
```

If you need more details on how to use Hugo modules, please read
the [Hugo documentation](https://gohugo.io/hugo-modules/use-modules/).

### Option 2: Cloning/Copying the Theme Files

Use this option if you want to directly customize and maintain your own copy of the theme.

- In the `my-new-blog/config.toml` file, uncomment the `theme` property for **Option 2**, and
  comment out the `theme` property for **Option 1**:

```toml
# Option 1 (recommended): adding the theme as a hugo module
# theme = "gitlab.com/enmanuelmoreira/BlogIT/v2"

# Option 2: cloning/copying the theme files
theme = "BlogIT"
```

- Copy cloned (or unzipped) theme files in previous step to the `my-new-blog/themes` directory:

```shell
cp -r BlogIT my-new-blog/themes/BlogIT
```

**Important:** Do NOT change the name of the `themes/BlogIT` folder in your site's root.
Renaming this folder will break your site.

Many new features and configurations have been added to the BlogIT theme, check the [changelog](https://gitlab.com/enmanuelmoreira/BlogIT/blob/main/CHANGELOG.md) and [documentation](https://hugoblogit.netlify.app/) for more information.

## [Demo Site](https://hugoblogit.netlify.app/)

To see this theme in action, here is a live [demo site](https://hugoblogit.netlify.app/) which is rendered with **BlogIT** theme.

## Why choose BlogIT

* Custom **Header**
* Custom **CSS Style**
* A new **home page**, compatible with the latest version of Hugo
* A lot of **style detail adjustments,** including color, font size, margins, code preview style
* More readable **dark mode**
* Some beautiful **CSS animations**
* Easy-to-use and self-expanding **table of contents**
* More **social links**, **share sites** and **comment system**
* **Search** supported by [Lunr.js](https://lunrjs.com/) or [algolia](https://www.algolia.com/) or [Fuse.js](https://fusejs.io/)
* **Copy code** to clipboard with one click
* Extended Markdown syntax for **[Font Awesome](https://fontawesome.com/) icons**
* Extended Markdown syntax for **ruby annotation**
* Extended Markdown syntax for **fraction**
* **Mathematical formula** supported by [KaTeX](https://katex.org/)
* **Diagram syntax** shortcode supported by [mermaid](https://github.com/knsv/mermaid)
* **Interactive data visualization** shortcode supported by [ECharts](https://echarts.apache.org/)
* **Mapbox** shortcode supported by [Mapbox GL JS](https://docs.mapbox.com/mapbox-gl-js)
* Embedded **music player** supported by [APlayer](https://github.com/MoePlayer/APlayer) and [MetingJS](https://github.com/metowolf/MetingJS)
* **Bilibili** player supported
* Kinds of **admonitions** shortcode supported
* Custom style shortcodes supported
* **CDN** for all third-party libraries supported
* **Mulitple Authors** supported
* **PJAX** supported
* ...

In short,
if you prefer the design language and freedom of the BlogIT theme,
if you want to use the extended Font Awesome icons conveniently,
if you want to embed mathematical formulas, flowcharts, music or Bilibili videos in your posts,
the BlogIT theme may be more suitable for you.

## Features

### Performance and SEO

* Optimized for **performance**: 99/100 on mobile and 100/100 on desktop in [Google PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights)
* Optimized SEO performance with a correct **SEO SCHEMA** based on JSON-LD
* **[Google Analytics](https://analytics.google.com/analytics)** supported
* **[Fathom Analytics](https://usefathom.com/)** supported
* **[Baidu Analytics](https://tongji.baidu.com/)** supported
* **[Umami Analytics](https://umami.is/)** supported
* **[Plausible Analytics](https://plausible.io/)** supported
* Search engine **verification** supported (Google, Bind, Yandex and Baidu)
* **CDN** for third-party libraries supported
* Automatically converted images with **Lazy Load** by [lazysizes](https://github.com/aFarkas/lazysizes)

### Appearance and Layout

* **Responsive** layout
* **Light/Dark/Black** mode
* Globally consistent **design language**
* **Pagination** supported
* Easy-to-use and self-expanding **table of contents**
* **Multilanguage** supported and i18n ready
* Beautiful **CSS animation**
* Brand new **Page Layout** optimized for wide screens
* Compatibility for **PWA**

### Social and Comment Systems

* **Gravatar** supported by [Gravatar](https://gravatar.com)
* Local **Avatar** supported
* Up to **64** social links supported
* Up to **28** share sites supported
* **Disqus** comment system supported by [Disqus](https://disqus.com)
* **Gitalk** comment system supported by [Gitalk](https://github.com/gitalk/gitalk)
* **Valine** comment system supported by [Valine](https://valine.js.org/)
* **Waline** comment system supported by [Waline](https://waline.js.org/)
* **Facebook comments** system supported by [Facebook](https://developers.facebook.com/docs/plugins/comments/)
* **Telegram comments** system supported by [Telegram Comments](https://comments.app/)
* **Commento** comment system supported by [Commento](https://commento.io/)
* **Utterances** comment system supported by [Utterances](https://utteranc.es/)
* **Twikoo** comment system supported by [Twikoo](https://twikoo.js.org/)
* **Vssue** comment system supported by [Vssue](https://vssue.js.org/)
* **Remark42** comment system supported by[Remark42](https://remark42.com/)
* **giscus** comment system supported by [giscus](https://giscus.app/)

### Extended Features

* **Search** supported by [Lunr.js](https://lunrjs.com/) or [algolia](https://www.algolia.com/) or [Fuse.js](https://fusejs.io/)
* **Twemoji** supported
* Automatically **highlighting** code
* **Copy code** to clipboard with one click
* **Images gallery** supported by [lightgallery.js](https://github.com/sachinchoolur/lightgallery.js)
* Extended Markdown syntax for **[Font Awesome](https://fontawesome.com/) icons**
* Extended Markdown syntax for **ruby annotation**
* Extended Markdown syntax for **fraction**
* **Mathematical formula** supported by [KaTeX](https://katex.org/)
* **Diagrams** shortcode supported by [mermaid](https://github.com/knsv/mermaid)
* **Interactive data visualization** shortcode supported by [ECharts](https://echarts.apache.org/)
* **Mapbox** shortcode supported by [Mapbox GL JS](https://docs.mapbox.com/mapbox-gl-js)
* **Music player** shortcode supported by [APlayer](https://github.com/MoePlayer/APlayer) and [MetingJS](https://github.com/metowolf/MetingJS)
* **Bilibili player** shortcode
* Kinds of **admonitions** shortcode
* **Custom style** shortcode
* **Custom script** shortcode
* **Custom friend** shortcode
* **Animated typing** supported by [TypeIt](https://typeitjs.com/)
* **Dynamic scroll** supported by native CSS
* **Cookie consent banner** supported by [cookieconsent](https://github.com/osano/cookieconsent)
* ...

## [Documentation](https://hugoblogit.netlify.app/categories/documentation/)

Build Documentation Locally:

```bash
hugo server --source=exampleSite
```

## Multilingual and i18n

BlogIT supports the following languages:

* English
* Simplified Chinese
* French
* Polish
* Brazilian Portuguese
* Italian
* Spanish
* German
* Serbian
* Russian
* Romanian
* Vietnamese
* [Contribute with a new language](https://gitlab.com/enmanuelmoreira/BlogIT/pulls)

[Languages Compatibility](https://hugoblogit.netlify.app/theme-documentation-basics/#language-compatibility)

## [Changelog](https://gitlab.com/enmanuelmoreira/BlogIT/blob/main/CHANGELOG.md)

## Questions, ideas, bugs, pull requests

All feedback is welcome! Head over to the [issue tracker](https://gitlab.com/enmanuelmoreira/BlogIT/issues).

## License

BlogIT is licensed under the **MIT** license. Check the [LICENSE file](https://gitlab.com/enmanuelmoreira/BlogIT/blob/main/LICENSE) for details.

Thanks to the authors of following resources included in the theme:

* [normalize.css](https://github.com/necolas/normalize.css)
* [Font Awesome](https://fontawesome.com/)
* [Simple Icons](https://github.com/simple-icons/simple-icons)
* [Animate.css](https://daneden.github.io/animate.css/)
* [autocomplete.js](https://github.com/algolia/autocomplete.js)
* [Lunr.js](https://lunrjs.com/)
* [algoliasearch](https://github.com/algolia/algoliasearch-client-javascript)
* [Fuse.js](https://fusejs.io/)
* [lazysizes](https://github.com/aFarkas/lazysizes)
* [object-fit-images](https://github.com/fregante/object-fit-images)
* [Twemoji](https://github.com/twitter/twemoji)
* [lightgallery.js](https://github.com/sachinchoolur/lightgallery.js)
* [clipboard.js](https://github.com/zenorocha/clipboard.js)
* [Sharer.js](https://github.com/ellisonleao/sharer.js)
* [TypeIt](https://typeitjs.com/)
* [KaTeX](https://katex.org/)
* [mermaid](https://github.com/knsv/mermaid)
* [ECharts](https://echarts.apache.org/)
* [Mapbox GL JS](https://docs.mapbox.com/mapbox-gl-js)
* [APlayer](https://github.com/MoePlayer/APlayer)
* [MetingJS](https://github.com/metowolf/MetingJS)
* [Gitalk](https://github.com/gitalk/gitalk)
* [Valine](https://valine.js.org/)
* [Waline](https://waline.js.org/)
* [Twikoo](https://twikoo.js.org/)
* [Vssue](https://vssue.js.org/)
* [Remark42](https://remark42.com/)
* [cookieconsent](https://github.com/osano/cookieconsent)
* [Pjax](https://github.com/PaperStrike/Pjax)
* [Topbar](https://github.com/buunguyen/topbar)

## Author

[enmanuelmoreira](https://itsimplenow.com)

## Sponsor

If you enjoy the theme, please set the star for the project or/and consider buying me a coffee ☕️.

* [PayPal](https://paypal.me/enmanuelmoreira)

Thanks! ❤️❤️❤️
